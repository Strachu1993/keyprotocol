package sample.parser;

import sample.dto.Message;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

	private Pattern pattern;
	private Matcher matcher;

	public Message getMessage(String toParse){
		Message m = null;
		matcher = doRegExp("(\\d*):(.*):(\\d*):(.*)", toParse);

		if(isExistGroup(4)){
			m = new Message();
			m.setDecision(Integer.parseInt(matcher.group(1)));
			m.setIp((matcher.group(2)));
			m.setPort((Integer.parseInt(matcher.group(3))));
			m.setMessage((matcher.group(4)));
		}
		return m;
	}

	public String[] getPassAndMessage(String toParse){
		String[] userMessage = new String[2];
		matcher = doRegExp("(.*):(.*)", toParse);
		if(isExistGroup(2)){
			userMessage[0] = matcher.group(1);
			userMessage[1] = matcher.group(2);
		}
		return userMessage;
	}

	private boolean isExistGroup(int count) {
		return matcher.find() && matcher.groupCount() == count;
	}

	private Matcher doRegExp(String reg, String text) {
		pattern = Pattern.compile(reg);
		return pattern.matcher(text);
	}

}
