package sample.tcp.server;

import sample.config.Config;
import sample.tcp.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Server {

    private Listener serverListener = null;
    private List<String> keys;

    public Server(){
        keys = new ArrayList<String>();
        setFewKeys();
        createListener();
        System.err.println("--> Serwer wystartował <--");
    }

    private void setFewKeys(){
        keys.add("adminKey");
        keys.add("juz tylko killer");
        keys.add("Mam ten kod");
        keys.add("cd-action");
        keys.add("klawiatura");
        keys.add("kapitan bomba");
        keys.add("kino");
        keys.add("cocacola");
        keys.add("pepski");
        keys.add("siemka");
        keys.add("ksiazka");
    }

    public boolean checkIsExistis(String key){
        return keys.stream()
                .filter(Predicate.isEqual(key))
                .count() > 0;
    }

    public boolean addNewKey(String key, String pass){
        if(Config.PASS.equals(pass)) {
            keys.add(key);
            return true;
        }
        return false;
    }

    private void createListener(){
        serverListener = new ServerListener(Config.SERVER_PORT, this);
    }
}
