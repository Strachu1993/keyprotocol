package sample.tcp.server;

import sample.dto.Message;
import sample.parser.Parser;
import sample.tcp.Listener;
import sample.tcp.Send;

import java.io.IOException;

public class ServerListener extends Listener {

    private Parser p = null;
    private Server server = null;

    public ServerListener(int port, Server server) {
        super(port);
        this.server = server;
        p = new Parser();
    }

    protected void processingMessage(){
        String mes = null;
        if (br != null)
            try {
                mes = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        processingUserData(mes);
    }

    private void processingUserData(String mes) {
        Message m = p.getMessage(mes);

        if (m != null) {
            if(m.getDecision() == 1) {
                processingCheck(m);
            }else if(m.getDecision() == 2){
                processingAdd(m);
            }else{
                System.err.println("Zły wybór: " + m.getIp() + ":" + m.getPort());
            }
        }
    }

    private void processingAdd(Message m) {
        String[] userMessage = p.getPassAndMessage(m.getMessage()); //0 - passs, 1 - message
        Send s = new Send(m.getPort(), m.getIp());
        s.sendMessage("Response: 'add': " + server.addNewKey(userMessage[1], userMessage[0]) + " - " + userMessage[1]);
    }

    private void processingCheck(Message m) {
        Send s = new Send(m.getPort(), m.getIp());
        s.sendMessage("Response 'check': " + server.checkIsExistis(m.getMessage()) + " - " + m.getMessage());
    }

}
