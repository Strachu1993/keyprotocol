package sample.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class Listener implements Runnable{

    protected ServerSocket serverSocket = null;
    protected Socket socket = null;
    protected BufferedReader br = null;
    protected InputStream in = null;
    protected int port;

    protected abstract void processingMessage();

    public Listener(int port){
        this.port = port;
        new Thread(this).start();
    }

    @Override
    public void run() {

        createServerSocket();

        while (true) {

            startListen();
            downloadInputStream();
            createOutStream();

            processingMessage();

            closeBufferedReader();
            closeInputStream();
            closeSocket();
        }
    }

    protected void createOutStream() {
        try {
            if (in != null)
                br = new BufferedReader(new InputStreamReader(in));
        } catch (Exception e) {
            System.out.println("Nie można utworzyć strumienia wyjściowego BufferedReader.");
        }
    }

    protected void downloadInputStream() {
        try {
            in = socket.getInputStream();
        } catch (IOException e) {
            System.out.println("Nie można pobrać strumienia wejściowego.");
        }
    }

    protected void startListen() {
        try {
            socket = serverSocket.accept();
        } catch (IOException e) {
            System.out.println("Nie można nawiązać połączenia z klientem.");
        }
    }

    protected void closeBufferedReader() {
        try {
            br.close();
        } catch (IOException e) {
            System.out.println("Nie można zamknąć strumieni wejściowych.");
        }
    }

    protected void closeInputStream() {
        try {
            in.close();
        } catch (IOException e) {
            System.out.println("Nie można zamknąć strumieni wejściowych.");
        }
    }

    protected void closeSocket() {
        try {
            socket.close();
        } catch (IOException e) {
            System.out.println("Nie można zamknąć gniazda sieciowego klienta.");
        }
    }

    protected void createServerSocket(){
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Nie można utworzyć gniazda serwera.");
        }
    }

}
