package sample.tcp.client;

import sample.config.Config;
import sample.tcp.Send;

public class Client {

    private Send send = null;

    public Client(){
        send = new Send(Config.SERVER_PORT, Config.IP_SERVER);
        System.err.println("--> Klient wystartował <--");
    }

    public void addNewKey(String pass, String key){
        send.sendMessage("2:" + Config.IP_CLIENT + ":" + Config.CLIENT_PORT + ":" + pass + ":" + key);
    }

    public void checkKey(String key){
        send.sendMessage("1:" + Config.IP_CLIENT  + ":" + Config.CLIENT_PORT + ":" + key);
    }

}
