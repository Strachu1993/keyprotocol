package sample.tcp.client;

import javafx.scene.control.TextArea;
import sample.tcp.Listener;

import java.io.IOException;

public class ClientListener extends Listener{ //  ctrl + alt + m - extract method

    private TextArea textArea;

    public ClientListener(int port, TextArea textArea) {
        super(port);
        this.textArea = textArea;
    }

    protected void processingMessage() {
        if (br != null)
            try {
                textArea.setText(textArea.getText() + br.readLine() + "\n");
                textArea.setScrollTop(textArea.getLength());
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

}
