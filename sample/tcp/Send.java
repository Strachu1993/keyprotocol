package sample.tcp;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Send {

    private Socket socket = null;
    private OutputStream os = null;
    private PrintWriter pw = null;
    private int port;
    private String ip;

    public Send(int port, String ip){
        this.port = port;
        this.ip = ip;
    }

    public void sendMessage(String text){
            createSocket();

            downloadStreamOut();
            createStreamOut();

            sendText(text);

            closeStreamOut();
            closeSocket();
    }

    private void createStreamOut(){
        try {
            if(os !=null)
                pw = new PrintWriter(os);
        }
        catch(Exception e) {
            System.out.println("Nie można utworzyć strumienia wyjściowego PrintWriter.");
        }
    }

    private void sendText(String text) {
        try {
            if(pw != null)
                pw.print(text);
        }
        catch(Exception e) {
            System.out.println("Nie można do strumienia wyjściowego zapisać danych.");
        }
    }

    private void closeStreamOut() {
        pw.close();
        try {
            os.close();
        }
        catch(IOException e) {
            System.out.println("Nie można zamknąć strumieni wyjściowych.");
        }
    }

    private void closeSocket() {
        try {
            socket.close();
        }
        catch(IOException e) {
            System.out.println("Nie można zamknąć połączenia.");
        }
    }

    private void downloadStreamOut() {
        try {
            os = socket.getOutputStream();
        }
        catch(IOException e) {
            System.out.println("Nie można pobrać strumienia wyjściowego.");
        }
    }

    private void createSocket() {
        try {
            socket = new Socket(ip, port);
        }
        catch(UnknownHostException e) {
            System.out.println("Nieznana nazwa hosta.");
        } catch(IOException e) {
            System.out.println("Nie można utworzyć gniazda ");
        }
    }

}
