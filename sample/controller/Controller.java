package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import sample.config.Config;
import sample.tcp.Listener;
import sample.tcp.client.Client;
import sample.tcp.client.ClientListener;

public class Controller {

    private Client client;
    private Listener clientListener = null;

//    @FXML // wstrzyknięcie
//    private Button buttonCheckKey;
//
//    @FXML
//    private Button buttonAddKey;

    @FXML
    private TextField fieldCheckKey;

    @FXML
    private TextField fieldPass;

    @FXML
    private TextField fieldNewKey;

    @FXML
    private TextArea textArea;

//    public Controller(){
//    }

    @FXML
    void initialize(){ // metoda wywoływana zaraz po konstruktorze, np metoda setText na buttonie w konstruktorze nie działa ponieważ button jest nullem, robimy to tutaj
        clientListener = new ClientListener(Config.CLIENT_PORT, textArea);
        client = new Client();
    }

    @FXML
    public void clickButtonCheckKey(){
        if(checkIsNotNullAndNotEmpty(fieldCheckKey.getText()))
            client.checkKey(fieldCheckKey.getText());
    }

    @FXML
    public void clickButtonAddKey(){
        if(checkIsNotNullAndNotEmpty(fieldPass.getText()) && checkIsNotNullAndNotEmpty(fieldNewKey.getText()))
            client.addNewKey(fieldPass.getText(), fieldNewKey.getText());
    }

    private boolean checkIsNotNullAndNotEmpty(String text){
        return !(text.equals("") && text != null);
    }

}
